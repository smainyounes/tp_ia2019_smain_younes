import pandas as pd
import numpy as np

dataset=pd.read_csv('breastCancer.csv')
dataset.info()

# dataset.describe()

# removing the id column
dataset = dataset.drop('id', axis='columns')

# removing the rows with '?'  
dataset = dataset.drop(dataset[dataset.bare_nucleoli == '?'].index)


# selecting the X and Y
X = dataset.iloc[:,:-1]
y = dataset['class']

# spliting the dataset into testing & training
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.4, stratify=y)

# running some methodes to determen the right parameters
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
grid_params={
    'n_neighbors':[3,5,7,9,11],
    'metric':['euclidean','manhattan']
}

gs=GridSearchCV(
    KNeighborsClassifier(),
    grid_params,
    verbose=1,
    cv=3,
    n_jobs=-1
)
gs_results=gs.fit(X_train, y_train)

print(gs_results.best_params_)

#training & testing
from sklearn.neighbors import KNeighborsClassifier
knn= KNeighborsClassifier(n_neighbors=9, metric='euclidean') 
knn.fit(X_train, y_train)
y_pred=knn.predict(X_test) 


# printing the precision and recall scores
from sklearn.metrics import precision_score, recall_score
print('precision=',precision_score(y_test,y_pred,average='weighted'))
print('recall=',recall_score(y_test,y_pred,average='weighted'))

# final results
from sklearn.metrics import  confusion_matrix
print(confusion_matrix(y_test, y_pred))